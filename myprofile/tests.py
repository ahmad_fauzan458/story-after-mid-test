from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import Client
from .views import index
from django.urls import resolve
from django.http import HttpRequest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.
class ProfilePageUnitTest(TestCase):

    def test_profile_page_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lading_page_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_profile_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Ahmad Fauzan Amirul Isnain', html_response)

    def test_profile_page_using_profile_template(self):
    	response = Client().get('/profile/')
    	self.assertTemplateUsed(response, 'profile.html')


class SeleniumProfileTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        cls.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        cls.browser.set_window_size(1440, 900)
        cls.browser.implicitly_wait(25) 
        cls.browser.get('%s%s' % (cls.live_server_url, '/profile/'))

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test1_change_to_theme2(self):
        side_nav_btn = self.browser.find_element_by_name('side-nav-btn')
        side_nav_btn.send_keys(Keys.RETURN)
        time.sleep(2) 
        body_color = self.browser.find_element_by_class_name("custom-body").value_of_css_property("background-color")
        side_nav_color = self.browser.find_element_by_class_name("side-navbar").value_of_css_property("background-color")
        top_nav_color = self.browser.find_element_by_class_name("top-navbar").value_of_css_property("background-color")
        panel_heading_color = self.browser.find_element_by_class_name("panel-heading-custom").value_of_css_property("background-color")
        display_header_img_1 = self.browser.find_element_by_class_name("header-img-1").value_of_css_property("display")
        display_header_img_2 = self.browser.find_element_by_class_name("header-img-2").value_of_css_property("display")
        nav_button_class = self.browser.find_element_by_class_name("nav-button").get_attribute("class")
        hover_nav_side = self.browser.find_element_by_class_name("side-navbar").get_attribute("class")
        hover_nav_top = self.browser.find_element_by_class_name("top-navbar").get_attribute("class")
        
        self.assertEqual("rgba(177, 227, 227, 1)", body_color)
        self.assertEqual("rgba(2, 54, 24, 1)", side_nav_color)
        self.assertEqual("rgba(2, 54, 24, 1)", top_nav_color)
        self.assertEqual("rgba(60, 179, 113, 1)", panel_heading_color)
        self.assertEqual("inline", display_header_img_2)
        self.assertEqual("none", display_header_img_1)
        self.assertIn("btn-success", nav_button_class)
        self.assertIn("side-navbar-theme2", hover_nav_side)
        self.assertIn("top-navbar-theme2", hover_nav_top)

    def test2_change_to_theme1(self):
        side_nav_btn = self.browser.find_element_by_name('side-nav-btn')
        side_nav_btn.send_keys(Keys.RETURN)
        time.sleep(2) 
        body_color = self.browser.find_element_by_class_name("custom-body").value_of_css_property("background-color")
        side_nav_color = self.browser.find_element_by_class_name("side-navbar").value_of_css_property("background-color")
        top_nav_color = self.browser.find_element_by_class_name("top-navbar").value_of_css_property("background-color")
        panel_heading_color = self.browser.find_element_by_class_name("panel-heading-custom").value_of_css_property("background-color")
        display_header_img_1 = self.browser.find_element_by_class_name("header-img-1").value_of_css_property("display")
        display_header_img_2 = self.browser.find_element_by_class_name("header-img-2").value_of_css_property("display")
        nav_button_class = self.browser.find_element_by_class_name("nav-button").get_attribute("class")
        hover_nav_side = self.browser.find_element_by_class_name("side-navbar").get_attribute("class")
        hover_nav_top = self.browser.find_element_by_class_name("top-navbar").get_attribute("class")

        self.assertEqual("rgba(218, 229, 255, 1)", body_color)
        self.assertEqual("rgba(0, 38, 79, 1)", side_nav_color)
        self.assertEqual("rgba(0, 38, 79, 1)", top_nav_color)
        self.assertEqual("rgba(63, 137, 224, 1)", panel_heading_color)
        self.assertEqual("none", display_header_img_2)
        self.assertEqual("inline", display_header_img_1)
        self.assertIn("btn-primary", nav_button_class)
        self.assertIn("side-navbar-theme1", hover_nav_side)
        self.assertIn("top-navbar-theme1", hover_nav_top)

    def test3_open_accordion(self):
        accordion = self.browser.find_element_by_name('experience-head')
        accordion.click()
        time.sleep(2) 
        height = self.browser.find_element_by_name("experience-body").value_of_css_property("max-height")
        self.assertEqual("204px", height)

    def test4_close_accordion(self):
        accordion = self.browser.find_element_by_name('experience-head')
        accordion.click()
        time.sleep(2) 
        height = self.browser.find_element_by_name("experience-body").value_of_css_property("max-height")
        self.assertEqual("0px", height)
