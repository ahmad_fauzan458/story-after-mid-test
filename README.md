# Repository Status Wall
Fakultas Ilmu Komputer, Universitas Indonesia, Semester Gasal 2018/2019
***


## Maintainer

1. Ahmad Fauzan Amirul Isnain - 1706979152

## Pipeline Status

[![pipeline status](https://gitlab.com/ahmad_fauzan458/story-after-mid-test/badges/master/pipeline.svg)](https://gitlab.com/ahmad_fauzan458/story-after-mid-test/commits/master)

## Code Coverage Status

[![coverage report](https://gitlab.com/ahmad_fauzan458/story-after-mid-test/badges/master/coverage.svg)](https://gitlab.com/ahmad_fauzan458/story-after-mid-test/commits/master)

## Heroku Link

https://ppw-e-fauzan-storyaftermidtest.herokuapp.com/