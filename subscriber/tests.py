from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.core.exceptions import ValidationError
from .views import index
from .models import Subscriber
from .forms import SubscribeForm
import time
import json
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class SubscribePageUnitTest(TestCase):

    def test_subscribe_page_url_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_subscribe_page_using_index_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, index)

    def test_subscribe_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Subscribe me', html_response)

    def test_forbidden_add_subscriber_with_no_data(self):
        response = Client().get('/subscribe/add_subscriber/')
        self.assertEqual(response.status_code, 403)

    def test_invalid_delete(self):
        response = Client().get('/subscribe/delete_subscriber?email=invalid&password=invalid')
        self.assertTrue(response.status_code, 200)

    def test_valid_delete(self):
        Subscriber.objects.create(name = 'name', email = 'email@gmail.com',  password = 'password')
        response = Client().get('/subscribe/delete_subscriber?email=email&password=password')
        self.assertTrue(response.status_code, 200)

    def test_email_already_registered(self):
        Subscriber.objects.create(name = 'name', email = 'email@gmail.com',  password = 'password')
        form = SubscribeForm(data = {'name': 'name', 'email':'email@gmail.com', 'password':'password'})
        self.assertFalse(form.is_valid())

class SubscribeModelUnitTest(TestCase):

    def test_model_can_create_new_subscriber(self):
        # Creating a new activity
        new_subscriber = Subscriber.objects.create(name='ahmad', email="aku@gmail.com", password="cobagan")
        # Retrieving all available activity
        counting_all_available_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_available_subscriber, 1)

    def test_model_not_create_invalid_subscriber(self):
        #test melebihi max length
        invalidName = "a" *71

         # Creating a new activity
        new_subscriber = Subscriber.objects.create(name=invalidName, email="aku@gmail.com", password="cobagan")

        self.assertRaises(ValidationError, new_subscriber.full_clean)

class SubscriberFormUnitTest(TestCase):
    
    def test_form_subscribe_input_has_css_classes(self):
        form = SubscribeForm()
        self.assertIn('class="form-control"', form.as_p())

    def test_subscribe_validation_for_blank_items(self):
        form = SubscribeForm(data={'name':'', 'email':'', 'password':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )    
  

class SeleniumSubscribeTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        cls.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        cls.browser.implicitly_wait(25)
        cls.browser.get('%s%s' % (cls.live_server_url, '/subscribe/'))

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test1_subscribe(self):
        name = self.browser.find_element_by_id('id_name')
        email = self.browser.find_element_by_id('id_email')
        password = self.browser.find_element_by_id('id_password')
        button = self.browser.find_element_by_id('submit_button')

        name.send_keys('Name')
        email.send_keys('email@gmail.com')
        password.send_keys('cobapass')
        time.sleep(5)

        button.send_keys(Keys.RETURN)
        time.sleep(5)

        infoFailed = self.browser.find_element_by_class_name("alert-danger").value_of_css_property("display")
        self.assertEqual(infoFailed, "none")
        infoSuccess = self.browser.find_element_by_class_name("alert-success").value_of_css_property("display")
        self.assertEqual(infoSuccess, "block") 
        time.sleep(5)

    def test2_invalid_subscribe(self):
        name = self.browser.find_element_by_id('id_name')
        email = self.browser.find_element_by_id('id_email')
        password = self.browser.find_element_by_id('id_password')
        button = self.browser.find_element_by_id('submit_button')
        toggle = self.browser.find_element_by_id('toggle-password')
        password_errors = self.browser.find_element_by_id('password_errors').text

        name.send_keys('NameAgain')
        email.send_keys('emailAgain@gmail.com')
        password.send_keys('c') #less than 8 char
        toggle.click()
        time.sleep(5)

        self.assertIn(password_errors, "Please lengthen your password to 8 characters or more (you are currently using 1 character)")
        self.assertEqual("true", button.get_attribute('disabled'))
        time.sleep(5)

    def test3_invalid_forced_subscribe(self):
        button = self.browser.find_element_by_id('submit_button')

        self.browser.execute_script("$('#submit_button').prop('disabled', false);")
        time.sleep(5)

        button.click()
        time.sleep(5)

        infoFailed = self.browser.find_element_by_class_name("alert-danger").value_of_css_property("display")
        self.assertEqual(infoFailed, "block")
        infoSuccess = self.browser.find_element_by_class_name("alert-success").value_of_css_property("display")
        self.assertEqual(infoSuccess, "none")  
        time.sleep(5)
    