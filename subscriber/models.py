from django.db import models
from django.core.validators import MinLengthValidator

class Subscriber(models.Model):
    name = models.CharField(max_length=70)
    email = models.EmailField(max_length=255, unique=True)
    password = models.CharField(max_length=32, validators=[MinLengthValidator(8)])
    

	
    