from django.urls import path
from .views import index, add_subscriber, check_email, get_subscriber, delete_subscriber

urlpatterns = [
    path('', index, name="index"),
    path('add_subscriber/', add_subscriber, name="add_subscriber"),
    path('check_email', check_email, name="check_email"),	
    path('get_subscriber', get_subscriber, name="get_subscriber"),
    path('delete_subscriber', delete_subscriber, name="delete_subscriber"),	
]
