from django.shortcuts import render
from .forms import SubscribeForm
from .models import Subscriber
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.core import serializers
response = {}


# Create your views here.
def index(request):
    response['SubscribeForm'] = SubscribeForm
    return render(request, 'subscribe.html', response)

def add_subscriber(request):
    form = SubscribeForm(request.POST)
    if form.is_valid():
        form.save()
        return JsonResponse({'success': True})
    response =  JsonResponse({'success': False, 'errors':form.errors})
    response.status_code = 403
    return response

def check_email(request):
    email = request.GET.get('email', '')
    response = {'valid' : None, 'exists': None, 'messages': []}
    try:
        validate_email(email)
    except ValidationError as error:
        response['valid'] = False
        response['messages'] = error.messages
    else:
        response['valid'] = True
        if Subscriber.objects.filter(email=email):
            response['exists'] = True
            response['messages'] = ["Subscriber with this email already exists"]
        else:
            response['exists'] = False
    return JsonResponse(response)

def get_subscriber(request):
    data = list(Subscriber.objects.values())
    data.reverse()
    response = {'data': data}
    return JsonResponse(response)

def delete_subscriber(request):
    email = request.GET.get('email', '')
    password = request.GET.get('password', '')
    response = {'EmailExists' : False, 'PasswordConfirm' : False}
    object = Subscriber.objects.filter(email=email)
    
    if object :
        response['EmailExists'] = True
        user_password = list(object.values_list('password', flat=True))[0]
        if password==user_password:
            response['PasswordConfirm'] = True
            object.delete()
            return JsonResponse(response)
    return JsonResponse(response)
