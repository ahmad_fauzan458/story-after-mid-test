isValidName = false;
isValidEmail = false;
isValidPassword = false;

$(document).ready(function() {
	$(".alert").hide();
	$('#id_name').on('input focusout', function(){
	    validateName();
	    toggleButton();
	});

	$('#id_email').on('input focusout', function(){
	    validateEmail();
	    toggleButton();
	});

	$('#id_password').on('input focusout', function(){
	    validatePassword();
	    toggleButton();
	});

	$("#toggle-password").click(function(){
		if ($('#id_password').attr('type') === 'password') $('#id_password').attr('type', 'text');
    	else $('#id_password').attr('type', 'password');
	});

	$("#toggle-confirm-password").click(function(){
		if ($('#password-unsub').attr('type') === 'password') $('#password-unsub').attr('type', 'text');
    	else $('#password-unsub').attr('type', 'password');
	});

	$('form').submit(function(event) {
        var formData = $(this).serializeArray();
        $.ajax({
            type: 'POST',
            url: 'add_subscriber/',
            data: formData,
            dataType: 'json',
        }).done(function(response) {
            $(".alert").hide();
            $(".alert-success").show();
            getSubscriberList();
        }).fail(function(response) {
            $(".alert").hide();
            $(".alert-danger").show();
        });
        event.preventDefault();
        resetForm();
    });
	
	initConfirmPassClick()
	getSubscriberList()
});

function resetForm(){
	$(':input','#subscribe-form')
	.not(':button, :submit, :reset, :hidden')
	.val('')
	.prop('checked', false)
	.prop('selected', false);

	isValidName = false;
	isValidEmail = false;
	isValidPassword = false;

    deactivateButton();
}

function toggleButton(){
	if (isValidName && isValidPassword && isValidEmail) activateButton();
	else deactivateButton();
}

function activateButton(){
	$('#submit_button').prop('disabled', false);
}

function deactivateButton(){
	$('#submit_button').prop('disabled', true);
}

function validateName(){
	var nameLength = $('#id_name').val().length;
	if (nameLength === 0){
		isValidName = false;
		$("#name_errors").html("<li>Name can't be empty</li>");
		return;
	}

	if (nameLength > 70){
		isValidName = false;
		$("#name_errors").html("<li>Name exceeds 70 character</li>");
		return;
	}
	$("#name_errors").empty();
	isValidName = true;
}

function validatePassword(){
	var passwordLength = $('#id_password').val().length;

	if (passwordLength === 0){
		isValidPassword = false;
		$("#password_errors").html("<li>Password can't be empty</li>");
		return;
	}

	if (passwordLength < 8){
		isValidPassword = false;
		$("#password_errors").html("<li>Please lengthen your password to 8 characters or more (you are currently using " 
			+ passwordLength + " character)</li>");
		return;
	}

	if (passwordLength > 32){
		isValidPassword = false;
		$("#password_errors").html("<li>Password exceeds 32 character</li>");
		return;
	}
	$("#password_errors").empty();
	isValidPassword = true;
}

function validateEmail(){
	email = $('#id_email').val()
	return $.ajax({
        type        : 'GET',
        url         : 'check_email?email=' + email,
        dataType    : 'json',
    }).then(function(response) {
    	$('#email_errors').empty();
        if (response.valid && !response.exists) {
            isValidEmail = true;
        } else {
        	var emailErrors = "";
            $.each(response.messages, function(i, message) {
				emailErrors = $("<li>").append(message);
            });
            emailErrors.appendTo($("#email_errors"));
            isValidEmail = false;
        }
	    toggleButton();
    });
}

function getSubscriberList(){
	return $.ajax({
        type        : 'GET',
        url         : 'get_subscriber'
    }).then(function(response) {
        constructSubscriberList(response.data);
    });
}

function deleteSubscriber(email, password){
	return $.ajax({
        type        : 'GET',
        url         : 'delete_subscriber?email=' + email + '&password=' + password,
    }).then(function(response) {
	   $('#modal-unsubscribe').modal('hide');
       getSubscriberList();
       if (response.EmailExists && response.PasswordConfirm){
       		showAnnouncementModal("You are now unsubscribed", "If at anytime you wish to join us again, please visit this page.")
       } else {
       		showAnnouncementModal("Wrong Password", "Please recheck your password")
       }
	   $('#modal-template').modal('show');
    });
}

function constructSubscriberList(data){
	$('#subscriber-list-content').empty();

	for( var i = 0; i < data.length; i++){ 
	   	subscriber = '<tr><th scope="row">' + data[i].name + '</th>';
	   	subscriber += '<td>' + data[i].email + '</td>';
	   	subscriber += '<td>' + createButton(i, data[i].email) + '</td></tr>';
	   	$(subscriber).appendTo('#subscriber-list-content');
	   	initUnsubscribeClick("unsubscribe_button_" + i);
	}
}

function createButton(id, value){
	return '<button class="unsubscribe-button save btn btn-primary center"' +
	'id="unsubscribe_button_' + id + '" value="' + value + '">Unsubscribe</button>';
}

function initUnsubscribeClick(id){
	$('#'+id).click(function(){
		showModal(id);
	});
}

function initConfirmPassClick(){
	confirmUnsub = $("#confirm-pass-unsub-btn");
	$(confirmUnsub).click(function(){
		deleteSubscriber(confirmUnsub.attr("value"), $("#password-unsub").val());
	});
}

function showModal(id){
	$("#confirm-pass-unsub-btn").attr("value", $('#'+id).attr("value"));
	$("#password-unsub").val("");
	$('#modal-unsubscribe').modal('show');
}

function showAnnouncementModal(title, p){
	$('#modal-template-title').html(title)
	$('#modal-template-p').html(p)
}