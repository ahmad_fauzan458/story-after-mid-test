from django import forms
from .models import Subscriber

class SubscribeForm(forms.ModelForm):

    name = forms.CharField(max_length=70, widget=forms.TextInput(
    	attrs={
    		'class': 'form-control',
    		'placeholder': 'Your name ...',
    	}
    ))

    email = forms.CharField(max_length=255, widget=forms.EmailInput(
    	attrs={
    		'class': 'form-control',
    		'placeholder': 'email@example.com',
    	}
    ))

    password = forms.CharField(max_length=32, widget=forms.PasswordInput(
    	attrs={
    		'class': 'form-control',
    		'placeholder': 'Your password ...',
    	}
    ))

    class Meta:
        model = Subscriber
        fields = '__all__'





