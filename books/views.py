from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

response = {}

def getJson(request, query):
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + query
	json_data = json.loads(requests.get(url).text)
	return JsonResponse(json_data)

def index(request):
	return render(request, 'books.html', response)