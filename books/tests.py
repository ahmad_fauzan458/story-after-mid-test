from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import Client
from .views import index, getJson
from django.urls import resolve
from django.http import HttpRequest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.
class BooksPageUnitTest(TestCase):

    def test_books_page_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)
    
    def test_getJson_page_url_is_exist(self):
        response = Client().get('/books/getJson/quilting/')
        self.assertEqual(response.status_code, 200)    

    def test_books_page_using_index_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, index)

    def test_getJson_using_getJson_func(self):
        found = resolve('/books/getJson/quilting/')
        self.assertEqual(found.func, getJson)

    def test_books_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Books', html_response)

    def test_books_page_using_profile_template(self):
    	response = Client().get('/books/')
    	self.assertTemplateUsed(response, 'books.html')


class SeleniumBooksTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        cls.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        cls.browser.get('%s%s' % (cls.live_server_url, '/books/'))
        cls.browser.maximize_window();
        time.sleep(25) 

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test1_change_color_favorite(self):
        time.sleep(5) 
        star = self.browser.find_element_by_class_name("fa-star")
        star.click()
        time.sleep(5) 
        star_color = star.value_of_css_property("color")
        
        self.assertEqual("rgba(255, 165, 0, 1)", star_color)

    def test2_change_color_unfavorite(self):
        star = self.browser.find_element_by_class_name("fa-star")
        star.click()
        time.sleep(5) 
        star_color = star.value_of_css_property("color")
        
        self.assertEqual("rgba(128, 128, 128, 1)", star_color)

    def test3_increment_counter(self):

        star_counter = self.browser.find_element_by_id("fav-count").text
        self.assertIn("0", star_counter)

        star = self.browser.find_element_by_class_name("fa-star")
        star.click()
        time.sleep(5) 

        star_counter = self.browser.find_element_by_id("fav-count").text
        self.assertIn("1", star_counter)

    def test4_decrement_counter(self):

        star_counter = self.browser.find_element_by_id("fav-count").text
        self.assertIn("1", star_counter)

        star = self.browser.find_element_by_class_name("fa-star")
        star.click()
        time.sleep(5) 

        star_counter = self.browser.find_element_by_id("fav-count").text
        self.assertIn("0", star_counter)