from django.urls import path
from .views import index,getJson

urlpatterns = [
	path('', index),
	path('getJson/<str:query>/', getJson, name='getJson'),
]