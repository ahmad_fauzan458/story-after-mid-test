function createStar(favId){
	return '<div class="fa fa-star" ' + 'id='+ favId + ' style="color: gray;"></div>'
};

function remove(favId, array){
	for( var i = 0; i < array.length; i++){ 
	   if ( array[i] === favId) {
	     array.splice(i, 1); 
	   }
	}
}

function has(favId, array){
	for( var i = 0; i < array.length; i++){ 
	   if ( array[i] === favId) {
	     return true; 
	   }
	}
	return false;
}

function loadLocalStar(){
	var array = JSON.parse(sessionStorage.fav);
	for( var i = 0; i < array.length; i++){ 
	   $("#"+array[i]).css("color", "orange");	
	}
	$('#fav-count').html("Favorite Count: "+ array.length);
}

function constructFavoritedId(){
	$('#fav-list-content').empty();

	var array = JSON.parse(sessionStorage.fav);
	var favorite = "";

	for( var i = 0; i < array.length; i++){ 
	   	favorite += '<tr><th scope="row">' + array[i] + '</th></tr>';
	}
	
	$(favorite).appendTo('#fav-list-content');	
}

function updateData(data){
	$(function(){
		$.ajax({
			url: "/user/update_data/" + data,
			success: function(result) {
        	},
		});
	});
}
$(document).ready(function(){
	$(function(){
		$.ajax({
			url: "/user/get_data/",
			success: function(result) {
				if (result.loggedIn){
	            	sessionStorage.fav = JSON.stringify(result.data);
	            	sessionStorage.loggedIn = true;
				} else {
					sessionStorage.fav = JSON.stringify(new Array());
					sessionStorage.loggedIn = false;
				}
        	},
		});
	});
	
	$('#search-button').click(function(){
		var query = $('#search-box').val();
		$(function(){
			$.ajax({
				url: "/books/getJson/" + query,
				success: function(result){
					$('#books-body').empty();
				    $.each(result.items, function(i, item) {
				        $('<tr>').append(
				        	$('<th scope="row">').text((i+1)),
				        	$('<td>').append('<img src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
				            $('<td>').text(item.volumeInfo.title),
				            $('<td>').text(item.volumeInfo.authors),
				            $('<td>').text(item.volumeInfo.publisher),
				            $('<td>').text(item.volumeInfo.publishedDate),
				            $('<td class="text-center">').append(createStar(item.id))
				        ).appendTo('#books-body');

						$("#"+item.id).click(function(event){
							var favSession = JSON.parse(sessionStorage.fav);

							if ($(this).css("color") === "rgb(128, 128, 128)"){
								$(this).css("color", "orange");
							}
							else {
								$(this).css("color", "gray");	
							}
							
							if (has(item.id, favSession)){
								remove(item.id, favSession);
								if(sessionStorage.loggedIn) updateData(item.id);
							}
							else {
						    	favSession.push(item.id);
								if(sessionStorage.loggedIn) updateData(item.id);
						    }
						    $('#fav-count').html("Favorite Count: "+ favSession.length);
						    sessionStorage.fav = JSON.stringify(favSession);
						    constructFavoritedId();
						});
					});
					loadLocalStar();
					constructFavoritedId();
				}
			});
		});
	});
	$('#search-box').val("quilting");
	$('#search-button').click();
	
	$('#show-fav-button').click(function(){
		if ($('#show-fav-button').html() == 'Show Favorite Books ID'){
			$('#show-fav-button').html('Hide Favorite Books ID');
			$("#fav-list").css("display", "block");
		} else {
			$('#show-fav-button').html('Show Favorite Books ID');
			$("#fav-list").css("display", "none");
		}
		
	});
});

