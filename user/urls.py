from django.urls import path
from .views import pull_data, push_data, update_data, get_data

urlpatterns = [
    path('pull_data/', pull_data, name="pull_data"),
    path('push_data/', push_data, name="push_data"),	
    path('update_data/<str:query>/', update_data, name="update_data"),	
    path('get_data/', get_data, name="get_data"),	

]
