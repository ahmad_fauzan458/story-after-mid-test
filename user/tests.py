from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import Client
from .views import pull_data, push_data, update_data, get_data
from django.contrib.auth.models import User
from django.urls import resolve
from django.http import HttpRequest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import json
# Create your tests here.
class UserPageUnitTest(TestCase):

    def test_user_get_data_when_not_log_in(self):
        response = Client().get('/user/get_data/')
        data = json.loads(response.content)
        self.assertEqual(data['loggedIn'], False)
    
    def test_user_get_data_when_logged_in(self):
        user = User.objects.create(username='username')
        user.set_password('password')
        user.save()
        login = self.client.login(username='username', password='password')
        info = ""
        try :
        	response = self.client.get('/user/get_data/')
        except :
        	info = "Already Logged in, but error because no books list in session"
        self.assertEqual(info, "Already Logged in, but error because no books list in session") 
    
    def test_pull_data_using_pull_data_func(self):
        found = resolve('/user/pull_data/')
        self.assertEqual(found.func, pull_data)

    def test_push_data_using_push_data_func(self):
        found = resolve('/user/push_data/')
        self.assertEqual(found.func, push_data)

    def test_get_data_using_get_data_func(self):
        found = resolve('/user/get_data/')
        self.assertEqual(found.func, get_data)
"""
    def test_getJson_using_getJson_func(self):
        found = resolve('/books/getJson/quilting/')
        self.assertEqual(found.func, getJson)

    def test_books_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Books', html_response)

    def test_books_page_using_profile_template(self):
    	response = Client().get('/books/')
    	self.assertTemplateUsed(response, 'books.html')
"""