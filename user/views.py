from django.shortcuts import render
from.models import User
from django.http import HttpResponseRedirect
import json
from django.http import JsonResponse

response = {}
# Create your views here.
def pull_data(request):
	if request.user.is_authenticated:
		object = User.objects.filter(username=request.user.username)
		if (object):
			object = User.objects.get(username=request.user.username)
			request.session['books_list'] = json.loads(object.books_list)
		else:	
			member = User.objects.create(username=request.user.username, books_list="[]")
			request.session['books_list'] = []
	return HttpResponseRedirect("../../landing_page")

def push_data(request):
	object = User.objects.get(username=request.user.username)
	object.books_list = json.dumps(request.session['books_list'])
	object.save()
	request.session.flush()
	return HttpResponseRedirect("../../logout")

def update_data(request, query):
	if query in request.session['books_list']:
		request.session['books_list'].remove(query)
		response['Remove'] = True
		response['Add'] = False
		request.session.modified = True
	else:
		request.session['books_list'].append(query)
		response['Remove'] = False
		response['Add'] = True
		request.session.modified = True
	return JsonResponse(response)

def get_data(request):
	if request.user.is_authenticated:
		response['loggedIn'] = True
		response['data'] = request.session['books_list']
	else:
		response['loggedIn'] = False
		response['data'] = []
	return JsonResponse(response)