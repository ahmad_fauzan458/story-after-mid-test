from django.db import models
from django.utils import timezone

class User(models.Model):
	username = models.CharField(max_length = 255)
	books_list = models.TextField()