# Generated by Django 2.1.1 on 2018-10-30 04:44

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('landing_page', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
