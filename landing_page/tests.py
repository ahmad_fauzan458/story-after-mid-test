from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Status
from .forms import StatusForm
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class LandingPageUnitTest(TestCase):

    def test_landing_page_url_is_exist(self):
        response = Client().get('/landing_page/')
        self.assertEqual(response.status_code, 200)

    def test_lading_page_using_index_func(self):
        found = resolve('/landing_page/')
        self.assertEqual(found.func, index)

    def test_landing_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Hello, Apa kabar?', html_response)

class StatusModelUnitTest(TestCase):

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_status = Status.objects.create(status = 'Dikejar-kejar deadline')
        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_model_not_create_invalid_status(self):
        #test melebihi max length
        test = "a" *301

         # Creating a new activity
        new_status = Status.objects.create(status = test)
        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

class StatusFormUnitTest(TestCase):
    
    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('placeholder="Apakah ada sesuatu yang terjadi ...?"', form.as_p())

    def test_status_validation_for_blank_items(self):
        form = StatusForm(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )    
    
    def test_lab5_post_success_and_render_the_result(self):
        test = 'masih dikejar deadline' 
        response_post = Client().post('/landing_page/add_status', {'status':test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/landing_page/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab5_post_error_and_render_the_result(self):
        #test melebihi max length
        test = "a" *301
        
        response_post = Client().post('/landing_page/add_status', {'status': test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/landing_page/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

class SeleniumLandingTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        cls.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        cls.browser.implicitly_wait(25)
        cls.browser.get('%s%s' % (cls.live_server_url, '/landing_page/'))

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test_submit_status(self):
        status_box = self.browser.find_element_by_name('status')
        status_box.send_keys('Coba Coba')
        status_box.submit()
        self.assertIn('Coba Coba', self.browser.page_source)
        time.sleep(5)
    
    def test_title_layout(self):
        titleLayout = self.browser.find_element_by_class_name("title").text
        self.assertIn("Hello, Apa kabar?", titleLayout)
    
    def test_main_form_layout(self):
        actualString = self.browser.find_element_by_class_name('main-form').text
        self.assertIn("Post", actualString)

    def test_body_background_color(self):
        bodyColor = self.browser.find_element_by_class_name("custom-body").value_of_css_property("background-color")
        self.assertEqual("rgba(218, 229, 255, 1)", bodyColor)

    def test_button_background_color(self):
        buttonColor = self.browser.find_element_by_class_name("btn-primary").value_of_css_property("background-color")
        self.assertEqual("rgba(0, 123, 255, 1)", buttonColor) #rgba(0, 123, 255, 1) = #0062cc
    
    def test1_change_to_theme2(self):
        side_nav_btn = self.browser.find_element_by_name('side-nav-btn')
        side_nav_btn.send_keys(Keys.RETURN)
        time.sleep(2) 
        body_color = self.browser.find_element_by_class_name("custom-body").value_of_css_property("background-color")
        side_nav_color = self.browser.find_element_by_class_name("side-navbar").value_of_css_property("background-color")
        top_nav_color = self.browser.find_element_by_class_name("top-navbar").value_of_css_property("background-color")
        nav_button_class = self.browser.find_element_by_class_name("nav-button").get_attribute("class")
        status_button_class = self.browser.find_element_by_class_name("status-button").get_attribute("class")
        hover_nav_side = self.browser.find_element_by_class_name("side-navbar").get_attribute("class")
        hover_nav_top = self.browser.find_element_by_class_name("top-navbar").get_attribute("class")
        
        self.assertEqual("rgba(177, 227, 227, 1)", body_color)
        self.assertEqual("rgba(2, 54, 24, 1)", side_nav_color)
        self.assertEqual("rgba(2, 54, 24, 1)", top_nav_color)
        self.assertIn("btn-success", nav_button_class)
        self.assertIn("btn-success", status_button_class)
        self.assertIn("side-navbar-theme2", hover_nav_side)
        self.assertIn("top-navbar-theme2", hover_nav_top)

    def test2_change_to_theme1(self):
        side_nav_btn = self.browser.find_element_by_name('side-nav-btn')
        side_nav_btn.send_keys(Keys.RETURN)
        time.sleep(2) 
        body_color = self.browser.find_element_by_class_name("custom-body").value_of_css_property("background-color")
        side_nav_color = self.browser.find_element_by_class_name("side-navbar").value_of_css_property("background-color")
        top_nav_color = self.browser.find_element_by_class_name("top-navbar").value_of_css_property("background-color")
        nav_button_class = self.browser.find_element_by_class_name("nav-button").get_attribute("class")
        status_button_class = self.browser.find_element_by_class_name("status-button").get_attribute("class")
        hover_nav_side = self.browser.find_element_by_class_name("side-navbar").get_attribute("class")
        hover_nav_top = self.browser.find_element_by_class_name("top-navbar").get_attribute("class")

        self.assertEqual("rgba(218, 229, 255, 1)", body_color)
        self.assertEqual("rgba(0, 38, 79, 1)", side_nav_color)
        self.assertEqual("rgba(0, 38, 79, 1)", top_nav_color)
        self.assertIn("btn-primary", nav_button_class)
        self.assertIn("btn-primary", status_button_class)
        self.assertIn("side-navbar-theme1", hover_nav_side)
        self.assertIn("top-navbar-theme1", hover_nav_top)
